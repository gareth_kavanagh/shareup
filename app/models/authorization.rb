class Authorization < ActiveRecord::Base
	belongs_to :user

	def self.find_by_provider_and_uid(provider, uid)
  		where(provider: provider, uuid: uid).first
	end
end
